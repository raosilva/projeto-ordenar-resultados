<?php

$dns = "mysql:dbname=projeto_ordenar;host=localhost";
$dbuser = "admin";
$dbpass = "";

try {
    $pdo = new PDO($dns, $dbuser, $dbpass);
} catch (PDOException $e) {
    echo "ERRO: ".$e->getMessage();
    exit;
}

if(isset($_GET['order']) && !empty($_GET['order'])){
    $order = addslashes($_GET['order']);
    $sql = "SELECT * FROM users ORDER BY ".$order;
} else {
    $order = "";
    $sql = "SELECT * FROM users";
}

?>

<form method="GET">
    <select name="order" id="order" onchange="this.form.submit()">
        <option></option>
        <option value="name" <?php echo ($order=="name")?'selected="selected"':''; ?>>Por nome</option>
        <option value="age" <?php echo ($order=="age")?'selected="selected"':''; ?>>Por idade</option>
    </select>
</form>

<table border="1" width="400">

    <thead>
        <tr>
            <th>Nome</th>
            <th>Idade</th>
        </tr>
    </thead>

    <?php

        $sql = $pdo->query($sql);

        if($sql->rowCount() > 0){

            foreach ($sql->fetchAll() as $user):
    ?>

    <tbody>
        <tr>
            <td><?php echo $user['name'];?></td>
            <td><?php echo $user['age'];?></td>
        </tr>
    </tbody>
    <?php
    endforeach;
    }
    ?>
</table>